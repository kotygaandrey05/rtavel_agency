package agency;

public enum Status {

	IN_PROGRESS,
	COMPLETED,
	CANCELLED;
}