package agency.tourist;

public class TouristNotFoundExeption extends RuntimeException {

	public TouristNotFoundExeption(Long id) {
	    super("Could not find tourist " + id);
	  }
}