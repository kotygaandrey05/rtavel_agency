package agency.tourist;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class TouristNotFoundAdvice {

  @ResponseBody
  @ExceptionHandler(TouristNotFoundExeption.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  String touristNotFoundHandler(TouristNotFoundExeption exeption) {
    return exeption.getMessage();
  }
}
