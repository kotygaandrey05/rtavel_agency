package agency.order;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import agency.tour.Tour;
import agency.tourist.Tourist;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class OrderId implements Serializable{
	
	@ManyToOne
	private Tour tour;

	@ManyToOne
	private Tourist tourist;
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
        if (!(o instanceof OrderId)) return false;
        OrderId that = (OrderId) o;
        return Objects.equals(tour, that.tour) &&
                Objects.equals(tourist, that.tourist);
	}
	@Override
	public int hashCode() {
		return Objects.hash(tour, tourist);
	}

}
