package agency.order;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table (name = "orders")
public class Order {

	@EmbeddedId
	private OrderId id;
	
	public Order() {
	}
	public Order(OrderId id) {
		super();
		this.id = id;
	}
	
	public OrderId getId() {
        return id;
    }
    public void setId(OrderId id) {
        this.id = id;
    }


}
