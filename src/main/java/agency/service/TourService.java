package agency.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import agency.tour.Tour;
import agency.tour.TourRepository;

@Service("tourServise")
public class TourService {
	
	@Autowired TourRepository tourRepository;
	 
    public List<Tour> getAllTours()
    {
       List<Tour> list = new ArrayList<>();
       tourRepository.findAll().forEach(list::add);
       return list;
    }

    public Tour getTourById(Long id)
    {
        return tourRepository.findById(id).get();
    }

    public void saveOrUpdate(Tour tour)
    {
    	tourRepository.save(tour);
    }

    public void delete(Long id)
    {
    	tourRepository.deleteById(id);
    }
	
}