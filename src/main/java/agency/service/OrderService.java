package agency.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

import agency.order.Order;
import agency.order.OrderId;
import agency.order.OrderRepository;


@Service("orderServise")
public class OrderService {
	
	@Autowired OrderRepository orderRepository;
	
	public List<Order> getAllOrders()
    {
       List<Order> list = new ArrayList<>();
       orderRepository.findAll().forEach(list::add);
       return list;
    }
	
	public Order getOrderById(OrderId id)
    {
        return orderRepository.findById(id).get();
    }

    public void saveOrUpdate(Order order)
    {
    	orderRepository.save(order);
    }

    public void delete(OrderId id)
    {
    	orderRepository.deleteById(id);
    }

}
