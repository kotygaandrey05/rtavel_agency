package agency.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import agency.tourist.Tourist;
import agency.tourist.TouristRepository;

@Service("touristServise")
public class TouristService{

	@Autowired 
	TouristRepository touristRepository;
 
    public List<Tourist> getAllTourists()
    {
       List<Tourist> list = new ArrayList<>();
       touristRepository.findAll().forEach(list::add);
       return list;
    }

    public Tourist getTouristById(Long id)
    {
        return touristRepository.findById(id).get();
    }

    public void saveOrUpdate(Tourist tourist)
    {
    	touristRepository.save(tourist);
    }

    public void delete(Long id)
    {
    	touristRepository.deleteById(id);
    }
}