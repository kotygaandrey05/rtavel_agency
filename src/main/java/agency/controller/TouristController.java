package agency.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import agency.service.TouristService;
import agency.tourist.Tourist;


@RestController
public class TouristController {
	
	  @Autowired
	  TouristService touristService;

	  @GetMapping("/tourists")
	  private List<Tourist> getAllTourists()
	  {
	      return touristService.getAllTourists();
	  }

	  @GetMapping("/tourists/{id}")
	  private Tourist getTourist(@PathVariable("id") Long id)
	  {
	      return touristService.getTouristById(id);
	  }

	  @DeleteMapping("/tourists/{id}")
	  private void deleteTourist(@PathVariable("id") Long id)
	  {
	      touristService.delete(id);
	  }

	  @PostMapping("/tourists")
	  private Long saveTourist(@RequestBody Tourist tourist)
	  {
	      touristService.saveOrUpdate(tourist);
	       return tourist.getId();
	  }
	
	
}