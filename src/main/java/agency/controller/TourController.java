package agency.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import agency.service.TourService;
import agency.tour.Tour;


@RestController
public class TourController {

	  @Autowired
	  TourService tourService;

	  @GetMapping("/tours")
	  private List<Tour> getAllTour()
	  {
	      return tourService.getAllTours();
	  }

	  @GetMapping("/tours/{id}")
	  private Tour getTour(@PathVariable("id") Long id)
	  {
	      return tourService.getTourById(id);
	  }

	  @DeleteMapping("/tours/{id}")
	  private void deleteTour(@PathVariable("id") Long id)
	  {
	      tourService.delete(id);
	  }

	  @PostMapping("/tours")
	  private Long saveTour(@RequestBody Tour tour)
	  {
	      tourService.saveOrUpdate(tour);
	      return tour.getId();
	  }
	
}
