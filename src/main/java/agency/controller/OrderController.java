package agency.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import agency.order.Order;
import agency.order.OrderId;
import agency.service.OrderService;;


@RestController
public class OrderController {

	  @Autowired
	  OrderService orderService;

	  @GetMapping("/orders")
	  private List<Order> getAllOrder()
	  {
	      return orderService.getAllOrders();
	  }

	  @GetMapping("/orders/{id}")
	  private Order getOrder(@PathVariable("id") OrderId id)
	  {
	      return orderService.getOrderById(id);
	  }

	  @DeleteMapping("/orders/{id}")
	  private void deleteOrder(@PathVariable("id") OrderId id)
	  {
	      orderService.delete(id);
	  }

	  @PostMapping("/orders")
	  private OrderId saveOrder(@RequestBody Order order)
	  {
	       orderService.saveOrUpdate(order);
	       return order.getId();
	  }


}