package agency.tour;

public class TourNotFoundExeption extends RuntimeException {

	public TourNotFoundExeption(Long id) {
	    super("Could not find tour " + id);
	  }
}