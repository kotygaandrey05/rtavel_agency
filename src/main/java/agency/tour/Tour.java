package agency.tour;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Tour")
public class Tour {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	private String tourName;
	private String typeOfToure;
	private String whatATour;
	
	public Tour() {}
	public Tour(Long id, String tourName, String typeOfToure, String whatATour) {
		super();
		this.id = id;
		this.tourName = tourName;
		this.typeOfToure = typeOfToure;
		this.whatATour = whatATour;
	}
	
	public Long getId() {
        return id;
    }
	public void setId(Long id) {
        this.id = id;
    }
	public String getTourName() {
		return tourName;
	}
	public void setTourName(String tourName) {
		this.tourName = tourName;
	}
	public String getTypeOfToure() {
		return typeOfToure;
	}
	public void setTypeOfToure(String typeOfToure) {
		this.typeOfToure = typeOfToure;
	}
	public String getWhatATour() {
		return whatATour;
	}
	public void setWhatATour(String whatATour) {
		this.whatATour = whatATour;
	}
	
}
